import { setLocale } from "yup";

setLocale({
  mixed: {
    required: ({ path }) => `${path} é requerido`,
  },
  string: {
    max: ({ path, max }) => `${path} precisa ter no máximo ${max} caracteres`,
  },
  number: {
    min: ({ path, min }) => `${path} precisa ser no mínimo ${min}`,
    max: ({ path, max }) => `${path} precisa ser no mínimo ${max}`,
  },
});

// const ptBR = {
//   mixed: {
//     required: ({ path }) => `${path} é requerido`,
//   },
//   string: {
//     max: ({ path, max }) => `${path} precisa ter no máximo ${max} caracteres`,
//   },
//   number: {
//     min: ({ path, min }) => `${path} precisa ser no mínimo ${min}`,
//     max: ({ path, max }) => `${path} precisa ser no mínimo ${max}`,
//   },
// };

// setLocale(ptBR);

export * from "yup";
