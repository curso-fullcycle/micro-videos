import {RouteProps} from "react-router-dom";
import Dashboard from "../pages/Dashboard";
import CategoryList from "../pages/Category/PageList";
import CategoryForm from "../pages/Category/PageForm";
import CastMemberList from "../pages/Cast-Member/PageList";
import CastMemberForm from "../pages/Cast-Member/PageFrom";
import GenreList from "../pages/Genre/PageList";
import GenreForm from "../pages/Genre/PageForm";
// import VideoList from "../pages/video/PageList";
// import VideoForm from "../pages/video/PageForm";
// import UploadPage from "../pages/uploads";
// import Login from "../pages/Login";

export interface MyRouteProps extends RouteProps {
    name: string;
    label: string;
    auth?: boolean;
}

const routes: MyRouteProps[] = [
    // {
    //     name: 'login',
    //     label: 'Login',
    //     path: '/login',
    //     component: Login,
    //     exact: true,
    //     auth: false
    // },
    {
        name: 'dashboard',
        label: 'Dashboard',
        path: '/',
        component: Dashboard,
        exact: true,
        auth: true
    },
    {
        name: 'categories.list',
        label: 'Categorias',
        path: '/categories',
        component: CategoryList,
        exact: true,
        auth: true
    },
    {
        name: 'categories.create',
        label: 'Criar categoria',
        path: '/categories/create',
        component: CategoryForm,
        exact: true,
        auth: true
    },
    {
        name: 'categories.edit',
        label: 'Editar categoria',
        path: '/categories/:id/edit',
        component: CategoryForm,
        exact: true,
        auth: true
    },
    {
        name: 'cast_members.list',
        label: 'Membros de elenco',
        path: '/Cast-Members',
        component: CastMemberList,
        exact: true,
        auth: true
    },
    {
        name: 'cast_members.create',
        label: 'Criar membro de elenco',
        path: '/Cast-Members/create',
        component: CastMemberForm,
        exact: true,
        auth: true
    },
    {
        name: 'cast_members.edit',
        label: 'Editar membro de elenco',
        path: '/Cast-Members/:id/edit',
        component: CastMemberList,
        exact: true,
        auth: true
    },
    {
        name: 'genres.list',
        label: 'Gêneros',
        path: '/genres',
        component: GenreList,
        exact: true,
        auth: true
    },
    {
        name: 'genres.create',
        label: 'Criar gêneros',
        path: '/genres/create',
        component: GenreForm,
        exact: true,
        auth: true
    },
    {
        name: 'genres.edit',
        label: 'Editar gênero',
        path: '/genres/:id/edit',
        component: GenreList,
        exact: true,
        auth: true
    },
    // {
    //     name: 'videos.list',
    //     label: 'Listar vídeos',
    //     path: '/videos',
    //     component: VideoList,
    //     exact: true,
    //     auth: true
    // },
    // {
    //     name: 'videos.create',
    //     label: 'Criar vídeos',
    //     path: '/videos/create',
    //     component: VideoForm,
    //     exact: true,
    //     auth: true
    // },
    // {
    //     name: 'videos.edit',
    //     label: 'Editar vídeo',
    //     path: '/videos/:id/edit',
    //     component: VideoForm,
    //     exact: true,
    //     auth: true
    // },
    // {
    //     name: 'uploads',
    //     label: 'Uploads',
    //     path: '/uploads',
    //     component: UploadPage,
    //     exact: true,
    //     auth: true
    // }
];

export default routes;
