 
import React, {useState, useEffect} from 'react';
import { httpVideo } from '../../util/http';
import {CastMember} from '../../util/models';

import format from 'date-fns/format'
import parseISO from 'date-fns/parseISO';
import DefaultTable, { TableColumn } from '../../components/Table'

const CastMemberTypeMap = {
  1: 'Diretor',
  2: 'Ator'
};

const columnsDefinition: TableColumn[] = [
  {
    name: 'id',
    label: 'ID',
    width: '30%',
    options: {
      sort: false
    }
  },
  {
    name: "name",
    label: "Nome",
    width: '43%',
  },
  {
    name: "type",
    label: "Tipo",
    options: {
      customBodyRender(value, tableMeta, updateVAlue){
        return CastMemberTypeMap[value];
      }
    },
    width: '4%',
  },
  {
    name: "created_at",
    label: "Criado em",
    options: {
      customBodyRender(value){
        return <span>{format(parseISO(value), 'dd/MM/yyyy')}</span>
      }
    },
    width: '10%',
  },
  {
    name: "actions",
    width: '13%',
    label: "Ações",
  }
];

const Table = () => {

  const [data, setData] = useState<CastMember[]>([]);

  useEffect(() => {
    httpVideo.get('cast_members').then(
      response => setData(response.data.data)
    )
  },[]);

  return (
    <DefaultTable
      title="Listagem de membros de elenco"
      columns={columnsDefinition}
      data={data}
    />
  );
};

export default Table;