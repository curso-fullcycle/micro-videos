/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';

import Button, { ButtonProps } from '@material-ui/core/Button';
import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup, TextField, makeStyles, Theme, Box, FormHelperText } from '@material-ui/core';

import { Controller, useForm } from 'react-hook-form';
import castMemberHttp from '../../util/http/cast-member-http';
import * as yup from '../../util/vendor/yup';
import { yupResolver } from "@hookform/resolvers/yup";
import { useParams, useHistory } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { DefaultForm } from '../../components/DefaultForm';

const useStyles = makeStyles((theme: Theme) => {
  return {
    submit: {
      margin: theme.spacing(1)
    }
  }
});

const validationSchema = yup.object().shape({
  name: yup.string().label('Nome').required().max(255),
  type: yup.number().label('Tipo').required()
})

export const Form = () => {

  const {
    register,
    setValue,
    handleSubmit,
    getValues,
    reset,
    formState: { errors },
    control,
    watch} = useForm({
    resolver: yupResolver(validationSchema)
  });

  const classes = useStyles();
  const snackbar = useSnackbar();
  const history = useHistory();
  const {id}: any = useParams();
  const [castMember, setCastMamber] = useState<{id: string} | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const buttonProps: ButtonProps = {
    variant: "contained",
    className: classes.submit,
    color: 'secondary',
    disabled: loading
  } 

  useEffect(() => {

    if(!id){
      return;
    }

    async function getCastMember(){
      setLoading(true);
      try{
        const {data} = await castMemberHttp.get(id);
        setCastMamber(data.data);
        reset(data.data);
      }catch(error){
        console.log(error);
        snackbar.enqueueSnackbar('Não foi possível carregar as informações', {
          variant: 'error'
        })
      }finally{
        setLoading(false);
      }
    }

    getCastMember();

  }, []);

  useEffect(() => {
    register("type")
  }, [register]);

  async function onSubmit(formData, event) {
    setLoading(true);
    try {
        const http = !castMember
            ? castMemberHttp.create(formData)
            : castMemberHttp.update(castMember.id, formData);
        const {data} = await http;
        snackbar.enqueueSnackbar(
          `Membro de elenco ${data.data.name} salvo com sucesso`,
            {variant: 'success'}
        );
        setTimeout(() => {
            event
                ? (
                    id
                        ? history.replace(`/cast-members/${data.data.id}/edit`)
                        : history.push(`/cast-members/${data.data.id}/edit`)
                )
                : history.push('/cast-members')
        });
    } catch (error) {
        console.error(error);
        snackbar.enqueueSnackbar(
            'Não foi possível salvar o membro de elenco',
            {variant: 'error'}
        )
    }finally {
      setLoading(false);
    }
  }

  return (
    <DefaultForm GridItemProps={{xs: 12, md: 6}} onSubmit={handleSubmit(onSubmit)}>
      
      <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <TextField
            label="Nome"
            fullWidth
            variant="outlined"
            margin="normal"
            disabled={loading}
            error={errors.name !== undefined}
            helperText={errors.name && errors.name.message}
            {...field}
            InputLabelProps={{shrink: true}}
          />
        )}
      />

      <FormControl margin={"normal"} error={errors.type !== undefined} disabled={loading}>

        <FormLabel component="legend">Tipo</FormLabel>

        <RadioGroup
          name="type"
          onChange={(e) => {setValue('type', parseInt(e.target.value));}}
          value={watch('type') + ""} //Convertendo para string
        >

          <FormControlLabel value="1" control={<Radio color={"primary"}/>} label="Diretor"/>

          <FormControlLabel value="2" control={<Radio color={"primary"}/>} label="Ator"/>

        </RadioGroup>

        {
          errors.type && <FormHelperText id="type-helper-text">{errors.type.message}</FormHelperText>
        }

      </FormControl>

      <Box dir="rtl">
        <Button {...buttonProps}onClick={() => onSubmit(getValues(), null)}>Salvar</Button>
        <Button {...buttonProps} type="submit">Salvar e continuar editando</Button>        
      </Box>

    </DefaultForm>
  );
};