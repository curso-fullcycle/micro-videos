/* eslint-disable react-hooks/exhaustive-deps */

import React, {useState, useEffect, useRef} from 'react';
import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';
import categoryHttp from '../../util/http/category-http';
import {Category, ListResponse} from '../../util/models';
import { BadgeNo, BadgeYes } from '../../components/Badge';
import DefaultTable, {makeActionStyles, TableColumn} from '../../components/Table';
import {IconButton, MuiThemeProvider} from "@material-ui/core";
import {Link} from "react-router-dom";
import EditIcon from '@material-ui/icons/Edit';
import { useSnackbar } from 'notistack';
// import useFilter from "../../hooks/useFilter";

interface Pagination {
  page: number;
  total: number;
  per_page: number;
}

interface SearchState {
  search: string;
  pagination: Pagination;
}

const columnsDefinition: TableColumn[] = [
  {
    name: 'id',
    label: 'ID',
    width: '30%',
    options: {
      sort: false
    }
  },
  {
    name: "name",
    label: "Nome",
    width: '43%',
  },
  {
    name: "is_active",
    label: "Ativo",
    options: {
      customBodyRender(value){
        return value ? <BadgeYes /> : <BadgeNo/>
      }
    },
    width: '4%',
  },
  {
    name: "created_at",
    width: '10%',
    label: "Criado em",
    options: {
      customBodyRender(value){
        return <span>{format(parseISO(value), 'dd/MM/yyyy')}</span>
      }
    }
  },
  {
    name: "actions",
    label: "Ações",
    width: '13%',
    options: {
        sort: false,
        filter: false,
        customBodyRender: (value, tableMeta) => {
            return (
                <IconButton
                    color={'secondary'}
                    component={Link}
                    to={`/categories/${tableMeta.rowData[0]}/edit`}
                >
                    <EditIcon/>
                </IconButton>
            )
        }
    }
  }
];

const Table = () => {

  const [data, setData] = useState<Category[]>([]);
  const subscribed = useRef(true);
  const snackbar = useSnackbar();
  const [loading, setLoading] = useState<boolean>(false);
  const [searchState, setSearchState] = useState<SearchState>({
    search: 'teste',
    pagination: {
      page: 1,
      total: 0,
      per_page: 10

    }
  });

  useEffect(() => {
    console.log('executou');
    
    return () => console.log('desmontou') ;

  },[]);

  useEffect(() => {    
    subscribed.current = true;
    getData();
    return () => {
        subscribed.current = false;
    } 
  },[searchState]);

  async function getData(){
    setLoading(true);

      try {
        const {data} = await categoryHttp.list<ListResponse<Category>>({
          queryParams: {
            search: searchState.search
          }
        });

        if(subscribed.current){
          setData(data.data);
          console.log(data.data);
        } 

      } catch (error) {
        console.log(error);
        snackbar.enqueueSnackbar('Não foi possível carregar as informações', {
          variant: 'error'
        })
      } finally {
        setLoading(false);
      }
  }

  return (
    <MuiThemeProvider theme={makeActionStyles(columnsDefinition.length - 1)}>
      <DefaultTable
        title="Listagem de categorias"
        columns={columnsDefinition}
        data={data}
        loading={loading}
        options={{
          serverSide: true,
          searchText: searchState.search,
          responsive: "scrollMaxHeight",
          page: searchState.pagination.page,
          rowsPerPage: searchState.pagination.per_page,
          onSearchChange: (value) => setSearchState(prevState => ({
            ...prevState,
            search: value 
          })),
      }}
      />
    </MuiThemeProvider>
  );
};

export default Table;