/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';

import { Checkbox, FormControlLabel, TextField } from '@material-ui/core';

import { Controller, useForm } from 'react-hook-form';
import categoryHttp from '../../util/http/category-http';
import * as yup from '../../util/vendor/yup'
import { yupResolver } from "@hookform/resolvers/yup";
import { useParams, useHistory } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import SubmitActions from '../../components/SubmitActions';
import { DefaultForm } from '../../components/DefaultForm';

const validationSchema = yup.object().shape({
  name: yup.string().label('Nome').required().max(255)
})

export const Form = () => {

  const {
    register,
    setValue,
    handleSubmit,
    getValues,
    reset,
    formState: { errors },
    control,
    watch,
    trigger} = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: {
      is_active: true,
      name: '',
      description: ''
    }
  });

  const snackbar = useSnackbar();
  const history = useHistory();
  const {id}: any = useParams();
  const [category, setCategory] = useState<{id: string} | null>(null);
  const [loading, setLoading] = useState<boolean>(false);  

  useEffect(() => {

    if(!id){
      return;
    }

    (async function getCategory(){
      setLoading(true);
      try{
        const {data} = await categoryHttp.get(id);
        setCategory(data.data);
        reset(data.data);
      }catch(error){
        console.log(error);
        snackbar.enqueueSnackbar('Não foi possível carregar as informações', {
          variant: 'error'
        })
      }finally{
        setLoading(false);
      }
    })();

  }, []);

  useEffect(() => {
    register("is_active")
  }, [register]);

  async function onSubmit(formData, event) {
    setLoading(true);
    try {
        const http = !category
            ? categoryHttp.create(formData)
            : categoryHttp.update(category.id, formData);
        const {data} = await http;
        snackbar.enqueueSnackbar(
            'Categoria salva com sucesso',
            {variant: 'success'}
        );
        setTimeout(() => {
            event
                ? (
                    id
                        ? history.replace(`/categories/${data.data.id}/edit`)
                        : history.push(`/categories/${data.data.id}/edit`)
                )
                : history.push('/categories')
        });
    } catch (error) {
        console.error(error);
        snackbar.enqueueSnackbar(
            'Não foi possível salvar a categoria',
            {variant: 'error'}
        )
    }finally {
      setLoading(false);
    }
}

  return (
        <DefaultForm GridItemProps={{xs: 12, md: 6}} onSubmit={handleSubmit(onSubmit)}>

          <Controller
            name="name"
            control={control}
            render={({ field }) => (
              <TextField
                label="Nome"
                fullWidth
                variant="outlined"
                margin="normal"
                disabled={loading}
                error={errors.name !== undefined}
                helperText={errors.name && errors.name.message}
                {...field}
                InputLabelProps={{shrink: true}}
              />
            )}
          />

          <Controller
            name="description"
            control={control}
            render={({ field }) => (
              <TextField
                label="Descrição"
                multiline
                rows="4"
                fullWidth
                variant="outlined"
                margin="normal"
                disabled={loading}
                error={errors.description !== undefined}
                helperText={errors.description && errors.description.message}
                {...field}
                InputLabelProps={{shrink: true}}
              />
            )}
          />

          <FormControlLabel
            disabled={loading}
            control={
              <Checkbox
              color={"primary"}
              defaultChecked
              onChange={() => setValue('is_active', !getValues()['is_active'])}
              checked={watch('is_active')}/>
            }
            label={'Ativo'}
            labelPlacement={'end'}
          />

          {/* <SubmitActions disableButtons={loading} handleSave={() => onSubmit(getValues(), null)}/> */}

          <SubmitActions 
            disableButtons={loading} 
            handleSave={() => 
              trigger().then(isValid => {
                isValid && onSubmit(getValues(), null)
              })
            }
          />

        </DefaultForm>
  );
};
