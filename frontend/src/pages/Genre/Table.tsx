 
import { MUIDataTableColumn } from 'mui-datatables';
import React, {useState, useEffect} from 'react';
import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';
import genreHttp from '../../util/http/genre-http';
import { BadgeNo, BadgeYes } from '../../components/Badge';
import {Genre} from "../../util/models";
import DefaultTable from '../../components/Table'


const columnsDefinition: MUIDataTableColumn[] = [
  {
    name: "name",
    label: "Nome"
  },
  {
    name: "categories",
    label: "Categorias",
    options: {
      customBodyRender(value){
        return value ? <BadgeYes /> : <BadgeNo/>
      }
    }
  },
  {
    name: "created_at",
    label: "Criado em",
    options: {
      customBodyRender(value){
        return <span>{format(parseISO(value), 'dd/MM/yyyy')}</span>
      }
    }
  }
];

const Table = () => {

  const [data, setData] = useState<Genre[]>([]);

  useEffect(() => {

    let isSubscribed = true;

    (async () => {

      const {data} = await genreHttp.list();
      
      if(isSubscribed){
        setData(data.data);
      }
    })();

    return () => {
      isSubscribed = false;
    }
    
  },[]);

  return (
    <DefaultTable
      title="Listagem de generos"
      columns={columnsDefinition}
      data={data}
    />
  );
};

export default Table;